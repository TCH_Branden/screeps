import harvester
import builder
# defs is a package which claims to export all constants and some JavaScript objects, but in reality does
#  nothing. This is useful mainly when using an editor like PyCharm, so that it 'knows' that things like Object, Creep,
#  Game, etc. do exist.
from defs import *

# These are currently required for Transcrypt in order to use the following names in JavaScript.
# Without the 'noalias' pragma, each of the following would be translated into something like 'py_Infinity' or
#  'py_keys' in the output file.
__pragma__('noalias', 'name')
__pragma__('noalias', 'undefined')
__pragma__('noalias', 'Infinity')
__pragma__('noalias', 'keys')
__pragma__('noalias', 'get')
__pragma__('noalias', 'set')
__pragma__('noalias', 'type')
__pragma__('noalias', 'update')

def manage_creeps():
    # # clean dead creeps from memory.
    # for creep in Memory.creeps:
    #     if creep not in Game.creeps:
    #         del memory.creeps[creeps]

    # Run each creep
    for i, name in enumerate(Object.keys(Game.creeps)):
        creep = Game.creeps[name]
        if creep.memory.type == 'harvester':
            harvester.run_harvester(creep)
        elif creep.memory.type == 'builder':
            builder.run_builder(creep)

def manage_spawns(max_harvesters=10, max_builders=5):
    # Run each spawn
    for name in Object.keys(Game.spawns):
        spawn = Game.spawns[name]
        if not spawn.spawning:
            # Get the number of our creeps in the room.
            num_harvesters = _.sum(Game.creeps, lambda c: c.pos.roomName == spawn.pos.roomName and c.memory.type == 'harvester')
            num_builders = _.sum(Game.creeps, lambda c: c.pos.roomName == spawn.pos.roomName and c.memory.type == 'builder')
            # If there are no creeps, spawn a creep once energy is at 250 or more
            print(name, '-', 'energy:', spawn.room.energyAvailable, 'managing:', num_harvesters, 'harvesters,', num_builders, 'builders')
            if num_harvesters < max_harvesters and spawn.room.energyAvailable >= 250:
                spawn.createCreep([WORK, CARRY, MOVE, MOVE], {'type': 'harvester'})
            elif num_builders < max_builders and spawn.room.energyAvailable >= 250:
                spawn.createCreep([WORK, CARRY, MOVE, MOVE], {'type': 'builder'})
