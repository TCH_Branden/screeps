
from defs import *

__pragma__('noalias', 'name')
__pragma__('noalias', 'undefined')
__pragma__('noalias', 'Infinity')
__pragma__('noalias', 'keys')
__pragma__('noalias', 'get')
__pragma__('noalias', 'set')
__pragma__('noalias', 'type')
__pragma__('noalias', 'update')


def run_builder(creep):
    """
    Runs a creep as a generic harvester.
    :param creep: The creep to run
    """

    # If we're full, stop filling up and remove the saved source
    if creep.memory.filling and _.sum(creep.carry) >= creep.carryCapacity:
        creep.memory.filling = False
        del creep.memory.source
    # If we're empty, start filling again and remove the saved target
    elif not creep.memory.filling and creep.carry.energy <= 0:
        creep.memory.filling = True
        del creep.memory.target

    if creep.memory.filling:
        # If we have a saved source, use it
        if creep.memory.source:
            source = Game.getObjectById(creep.memory.source)
        else:
            # Get a random new source and save it
            source = _.sample(creep.room.find(FIND_SOURCES))
            creep.memory.source = source.id
            print(creep.memory.type, creep.name, 'filling..')

        # If we're near the source, harvest it - otherwise, move to it.
        if creep.pos.isNearTo(source):
            result = creep.harvest(source)
            if result != OK:
                print("[{}] Unknown result from creep.harvest({}): {}".format(creep.name, source, result))
        else:
            creep.moveTo(source)
    else:
        # If we have a saved target, use it
        if creep.memory.target:
            target = Game.getObjectById(creep.memory.target)
        else:
            # Target a construction site.
            target = creep.pos.findClosestByRange(FIND_CONSTRUCTION_SITES)
            if target == None:
                target = _(creep.room.find(FIND_STRUCTURES)).filter(lambda s: (s.hits < s.hitsMax)).sample()
                creep.memory.repairMode = True
                if target == None:
                    print(creep.memory.type, creep.name, 'waiting..')
                else:
                    creep.memory.target = target.id
                    print(creep.memory.type, creep.name, 'repairing..')

            else:
                creep.memory.repairMode = False
                creep.memory.target = target.id
                print(creep.memory.type, creep.name, 'building..')

            # if target not None:
            #     creep.memory.target = target.id
            # else:
            #     target = _(creep.room.find(FIND_STRUCTURES)) \
            #         .filter(lambda s: (s.hits < s.hitsMax).sample()

        # If we are targeting a spawn or extension, we need to be directly next to it - otherwise, we can be 3 away.
        try:
            if target.structureType:
                is_close = creep.pos.isNearTo(target)
            else:
                is_close = creep.pos.inRangeTo(target, 3)
        except:
            del creep.memory.target

        if is_close:
            if creep.memory.repairMode:
                result = creep.repair(target)
            else:
                result = creep.build(target)
            if result == OK or result == ERR_FULL:
                del creep.memory.target
            else:
                print("[{}] Unknown result from creep.transfer({}, {}): {}".format(
                    creep.name, target, result))

        else:
            creep.moveTo(target)
