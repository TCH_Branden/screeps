	__nest__ (
		__all__,
		'manager', {
			__all__: {
				__inited__: false,
				__init__: function (__all__) {
					var harvester = __init__ (__world__.harvester);
					var builder = __init__ (__world__.builder);
					var manage_creeps = function () {
						var __iterable0__ = enumerate (Object.keys (Game.creeps));
						for (var __index0__ = 0; __index0__ < __iterable0__.length; __index0__++) {
							var __left0__ = __iterable0__ [__index0__];
							var i = __left0__ [0];
							var name = __left0__ [1];
							var creep = Game.creeps [name];
							if (creep.memory.type == 'harvester') {
								harvester.run_harvester (creep);
							}
							else if (creep.memory.type == 'builder') {
								builder.run_builder (creep);
							}
						}
					};
					var manage_spawns = function (max_harvesters, max_builders) {
						if (typeof max_harvesters == 'undefined' || (max_harvesters != null && max_harvesters .hasOwnProperty ("__kwargtrans__"))) {;
							var max_harvesters = 10;
						};
						if (typeof max_builders == 'undefined' || (max_builders != null && max_builders .hasOwnProperty ("__kwargtrans__"))) {;
							var max_builders = 5;
						};
						var __iterable0__ = Object.keys (Game.spawns);
						for (var __index0__ = 0; __index0__ < __iterable0__.length; __index0__++) {
							var name = __iterable0__ [__index0__];
							var spawn = Game.spawns [name];
							if (!(spawn.spawning)) {
								var num_harvesters = _.sum (Game.creeps, (function __lambda__ (c) {
									return c.pos.roomName == spawn.pos.roomName && c.memory.type == 'harvester';
								}));
								var num_builders = _.sum (Game.creeps, (function __lambda__ (c) {
									return c.pos.roomName == spawn.pos.roomName && c.memory.type == 'builder';
								}));
								print (name, '-', 'energy:', spawn.room.energyAvailable, 'managing:', num_harvesters, 'harvesters,', num_builders, 'builders');
								if (num_harvesters < max_harvesters && spawn.room.energyAvailable >= 250) {
									spawn.createCreep ([WORK, CARRY, MOVE, MOVE], {'type': 'harvester'});
								}
								else if (num_builders < max_builders && spawn.room.energyAvailable >= 250) {
									spawn.createCreep ([WORK, CARRY, MOVE, MOVE], {'type': 'builder'});
								}
							}
						}
					};
					__pragma__ ('<use>' +
						'builder' +
						'defs' +
						'harvester' +
					'</use>')
					__pragma__ ('<all>')
						__all__.manage_creeps = manage_creeps;
						__all__.manage_spawns = manage_spawns;
					__pragma__ ('</all>')
				}
			}
		}
	);
