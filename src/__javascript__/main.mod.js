	(function () {
		var harvester = __init__ (__world__.harvester);
		var builder = __init__ (__world__.builder);
		var m =  __init__ (__world__.manager);
		var main = function () {
			m.manage_creeps ();
			m.manage_spawns (3, 3);
		};
		module.exports.loop = main;
		__pragma__ ('<use>' +
			'builder' +
			'defs' +
			'harvester' +
			'manager' +
		'</use>')
		__pragma__ ('<all>')
			__all__.main = main;
		__pragma__ ('</all>')
	}) ();
