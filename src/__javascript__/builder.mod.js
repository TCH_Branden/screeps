	__nest__ (
		__all__,
		'builder', {
			__all__: {
				__inited__: false,
				__init__: function (__all__) {
					var run_builder = function (creep) {
						if (creep.memory.filling && _.sum (creep.carry) >= creep.carryCapacity) {
							creep.memory.filling = false;
							delete creep.memory.source;
						}
						else if (!(creep.memory.filling) && creep.carry.energy <= 0) {
							creep.memory.filling = true;
							delete creep.memory.target;
						}
						if (creep.memory.filling) {
							if (creep.memory.source) {
								var source = Game.getObjectById (creep.memory.source);
							}
							else {
								var source = _.sample (creep.room.find (FIND_SOURCES));
								creep.memory.source = source.id;
								print (creep.memory.type, creep.name, 'filling..');
							}
							if (creep.pos.isNearTo (source)) {
								var result = creep.harvest (source);
								if (result != OK) {
									print ('[{}] Unknown result from creep.harvest({}): {}'.format (creep.name, source, result));
								}
							}
							else {
								creep.moveTo (source);
							}
						}
						else {
							if (creep.memory.target) {
								var target = Game.getObjectById (creep.memory.target);
							}
							else {
								var target = creep.pos.findClosestByRange (FIND_CONSTRUCTION_SITES);
								if (target == null) {
									var target = _ (creep.room.find (FIND_STRUCTURES)).filter ((function __lambda__ (s) {
										return s.hits < s.hitsMax;
									})).sample ();
									creep.memory.repairMode = true;
									if (target == null) {
										print (creep.memory.type, creep.name, 'waiting..');
									}
									else {
										creep.memory.target = target.id;
										print (creep.memory.type, creep.name, 'repairing..');
									}
								}
								else {
									creep.memory.repairMode = false;
									creep.memory.target = target.id;
									print (creep.memory.type, creep.name, 'building..');
								}
							}
							try {
								if (target.structureType) {
									var is_close = creep.pos.isNearTo (target);
								}
								else {
									var is_close = creep.pos.inRangeTo (target, 3);
								}
							}
							catch (__except0__) {
								delete creep.memory.target;
							}
							if (is_close) {
								if (creep.memory.repairMode) {
									var result = creep.repair (target);
								}
								else {
									var result = creep.build (target);
								}
								if (result == OK || result == ERR_FULL) {
									delete creep.memory.target;
								}
								else {
									print ('[{}] Unknown result from creep.transfer({}, {}): {}'.format (creep.name, target, result));
								}
							}
							else {
								creep.moveTo (target);
							}
						}
					};
					__pragma__ ('<use>' +
						'defs' +
					'</use>')
					__pragma__ ('<all>')
						__all__.run_builder = run_builder;
					__pragma__ ('</all>')
				}
			}
		}
	);
